FROM jekyll/builder:3.8

ENV APP_HOME /srv/jekyll
#WORKDIR $APP_HOME
#ENV BUNDLE_GEMFILE=$APP_HOME/Gemfile \
#BUNDLE_JOBS=4

#ADD Gemfile* $APP_HOME/
COPY Gemfile .
RUN bundle check || bundle install

#CMD ["top"]
